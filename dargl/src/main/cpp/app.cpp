#include <windows.h>
#include <cstring>
#include <iomanip>
#include <sstream>
#include <stdio.h>
#include <string>
#include <tchar.h>
#include <vector>
#include <fstream>
#include <iostream>
#include <strsafe.h>
#include <filesystem>
#include <regex>

using namespace std;

//defined through gradle
//#define DARGL_LOG

string GetLastErrorAsString() {
    DWORD errorMessageID = ::GetLastError();
    if (errorMessageID == 0) {
        return string();
    }

    LPSTR messageBuffer = nullptr;

    size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

    string message(messageBuffer, size);

    LocalFree(messageBuffer);

    return message;
}

string escape(string source) {
    if (source.find(' ') == string::npos)
        return source;
    return "\"" + source + "\"";
}

filesystem::path getBinaryPath() {
    wchar_t szPath[MAX_PATH];
    GetModuleFileNameW(NULL, szPath, MAX_PATH);
    return filesystem::path{ szPath };
}

int main(int argc, char *argv[]) {
    filesystem::path selfPath = getBinaryPath();
    filesystem::path parentPath = selfPath.parent_path();
    vector<string> arguments(argv + 1, argv + argc);
#ifdef DARGL_LOG
    cout << "Using dargl binary wrapper (c) JFronny\n";
    cout << "Program name is: " << selfPath << "\n";
    cout << "Launched from: " << filesystem::current_path() << "\n";
#endif

    stringstream args;

    string originalBinary = selfPath.string() + ".original.exe";
    string argumentsSource = selfPath.string() + ".args.txt";

    args << escape(originalBinary);
#ifdef DARGL_LOG
    cout << "Using target binary " << originalBinary << " with additional argumenst from " << argumentsSource << "\n";
#endif

    ifstream argsfile(argumentsSource.c_str());
    string classpath;
    bool appendsClasspath = false;
    if (argsfile.is_open()) {
        string currentLine;
#ifdef DARGL_LOG
        cout << "Using arguments from file:\n";
#endif
        regex cdMatcher("\\$\\{cd\\}");
        bool isClasspath = false;
        while (getline(argsfile, currentLine)) {
            currentLine = regex_replace(escape(currentLine), cdMatcher, parentPath.string());
            if (currentLine == "-classpath") {
                isClasspath = true;
            }
            else if (isClasspath) {
                isClasspath = false;
                classpath = currentLine;
                appendsClasspath = true;
            }
#ifdef DARGL_LOG
            cout << "  " << currentLine << "\n";
#endif
            args << " " << currentLine;
        }
        argsfile.close();
    }
    else {
        cout << "Unable to open arguments file\n";
    }

    if (argc == 1) {
#ifdef DARGL_LOG
        cout << "No further arguments were passed\n";
#endif
    }
    else {
#ifdef DARGL_LOG
        cout << (argc - 1) << " arguments were passed:\n";
#endif
        bool isClasspath = false;
        for (string s : arguments) {
            if (appendsClasspath) {
                if (s == "-classpath") {
                    isClasspath = true;
                }
                else if (isClasspath) {
                    isClasspath = false;
                    appendsClasspath = false;
                    s += ";" + classpath;
                }
            }
#ifdef DARGL_LOG
            cout << "  " << escape(s) << "\n";
#endif
            args << " " << escape(s);
        }
    }

    STARTUPINFOA si;
    PROCESS_INFORMATION pi;

    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));

#ifdef DARGL_LOG
    cout << "Invoking and passing control to: " << args.str() << "\n";
#endif

    if (!CreateProcessA(NULL, (LPSTR)args.str().c_str(), NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {
        cout << "Could not create process (" << GetLastError() << "): " << GetLastErrorAsString() << "\n";
        return -1;
    }

    WaitForSingleObject(pi.hProcess, INFINITE);

    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);

    return 0;
}