package io.gitlab.jfronny.jfdk.test;

import java.util.Map;
import java.util.function.Function;

public class ValTest {
    public static void run() {
        val finalTest = "Some value";
        let normalTest = "Something else";
        normalTest = "This should work";
        boolean test = false;
        bool moskau = true;
        string thisIsAString = "Yay";
        string[] yes = new string[0];
        normalTest += thisIsAString;

        Map<bool, Function<bool, string>> fnc = Map.of(true, b -> b.toString(), false, b -> "yes");

        for (Map.Entry<bool, Function<bool, string>> entry : fnc.entrySet()) {
            yes(entry.getValue().apply(entry.getKey()));
        }
    }

    private static <T extends string> void yes(T... args) {
    }
}
