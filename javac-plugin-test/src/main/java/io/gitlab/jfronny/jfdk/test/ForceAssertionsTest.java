package io.gitlab.jfronny.jfdk.test;

public class ForceAssertionsTest {
    public static void run() {
        try {
            assert 5 == 4;
            throw new RuntimeException("ForceAssertions seems to be inactive");
        } catch (AssertionError e) {
            // This is expected
        }
    }
}
