package io.gitlab.jfronny.jfdk.test;

public class TestRunner {
    public static void main(String[] args) {
        ForceAssertionsTest.run();
        RomanTest.run();
        ValTest.run();
        System.out.println("Test execution was successful");
    }
}
