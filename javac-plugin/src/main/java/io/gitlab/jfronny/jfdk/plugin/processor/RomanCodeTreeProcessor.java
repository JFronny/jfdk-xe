package io.gitlab.jfronny.jfdk.plugin.processor;

import com.sun.source.util.Trees;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Names;
import io.gitlab.jfronny.jfdk.plugin.CodeTreeProcessor;
import io.gitlab.jfronny.jfdk.plugin.util.RomanNumber;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.util.Elements;

public class RomanCodeTreeProcessor extends CodeTreeProcessor {
    public RomanCodeTreeProcessor(TreeMaker make, Names names, Elements elements, Trees trees, ProcessingEnvironment processingEnv) {
        super(make, names, elements, trees, processingEnv, "Roman Numerals");
    }

    @Override
    public void visitIdent(JCTree.JCIdent tree) {
        String name = tree.getName().toString();
        if (RomanNumber.isRoman(name)) {
            result = make.Literal(RomanNumber.toInt(name));
            result.pos = tree.pos;
            tally++;
        } else super.visitIdent(tree);
    }
}
