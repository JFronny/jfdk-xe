package io.gitlab.jfronny.jfdk.plugin.util;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class ValueComparableMap<K extends Comparable<K>, V> extends TreeMap<K, V> {
    private final Map<K, V> valueMap;

    public ValueComparableMap(final Comparator<? super V> partialValueOrdering) {
        this(partialValueOrdering, new HashMap<>());
    }

    private ValueComparableMap(final Comparator<? super V> partialValueOrdering, HashMap<K, V> valueMap) {
        super((v1, v2) -> partialValueOrdering.compare(valueMap.get(v1), valueMap.get(v2)));
        this.valueMap = valueMap;
        this.putAll(valueMap);
    }

    @Override
    public V put(K key, V value) {
        if (valueMap.containsKey(key)) remove(key);
        valueMap.put(key,value);
        return super.put(key, value);
    }

    @Override
    public boolean containsKey(Object key) {
        return valueMap.containsKey(key);
    }

    @Override
    public V getOrDefault(Object key, V defaultValue) {
        return containsKey(key) ? get(key) : defaultValue;
    }

    public static <K extends Comparable<K>, V> Map<K, V> of(final Comparator<? super V> partialValueOrdering, Map<K, V> valueMap) {
        Map<K, V> map = new ValueComparableMap<>(partialValueOrdering);
        map.putAll(valueMap);
        return map;
    }
}
