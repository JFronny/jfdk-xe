package io.gitlab.jfronny.jfdk.plugin.processor;

import com.sun.source.util.Trees;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Names;
import io.gitlab.jfronny.jfdk.plugin.CodeTreeProcessor;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.util.Elements;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VariableTypeExpansionCodeTreeProcessor extends CodeTreeProcessor {
    private static final Pattern BOOL_PATTERN = Pattern.compile("(?<!\\w)bool(?!\\w)");
    private static final Pattern STRING_PATTERN = Pattern.compile("(?<!\\w)string(?!\\w)");

    public VariableTypeExpansionCodeTreeProcessor(TreeMaker make, Names names, Elements elements, Trees trees, ProcessingEnvironment processingEnv) {
        super(make, names, elements, trees, processingEnv, "Variable Type Expansion");
    }

    @Override
    public void visitVarDef(JCTree.JCVariableDecl tree) {
        if (tree.vartype != null) {
            log("Visiting ${tree.vartype} ${tree.getName()} (${tree.vartype.getClass()})");
        }
        if (tree.vartype instanceof JCTree.JCIdent ident) {
            String iName = ident.getName().toString();
            if ("val".equals(iName)) {
                log("Caught ident: val");
                tally++;
                tree.vartype = null;
                tree.mods.flags |= 16L; // final
            } else if ("let".equals(iName)) {
                log("Caught ident: let");
                tally++;
                tree.vartype = null;
            }
        }
        super.visitVarDef(tree);
    }

    @Override
    public void visitIdent(JCTree.JCIdent tree) {
        String iName = tree.getName().toString();
        log("Caught ident: $iName");
        Matcher m = BOOL_PATTERN.matcher(iName);
        if (m.find()) {
            log("Identified shorthand bool in $iName");
            tree.name = names.fromString(m.replaceAll(matchResult -> {
                tally++;
                return "Boolean";
            }));
        }
        m = STRING_PATTERN.matcher(iName);
        if (m.find()) {
            log("Identified shorthand string in $iName");
            tree.name = names.fromString(m.replaceAll(matchResult -> {
                tally++;
                return "String";
            }));
        }
        super.visitIdent(tree);
    }
}
