package io.gitlab.jfronny.jfdk.plugin;

import com.sun.source.util.Trees;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.Names;
import io.gitlab.jfronny.jfdk.plugin.processor.ForceAssertionsCodeTreeProcessor;
import io.gitlab.jfronny.jfdk.plugin.processor.RomanCodeTreeProcessor;
import io.gitlab.jfronny.jfdk.plugin.processor.VariableTypeExpansionCodeTreeProcessor;
import manifold.util.NecessaryEvilUtil;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import java.util.Set;

@SupportedAnnotationTypes("*")
@SupportedSourceVersion(SourceVersion.RELEASE_18)
public class ClassProcessor extends AbstractProcessor {
    private Trees trees;
    private Set<CodeTreeProcessor> processors;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        NecessaryEvilUtil.bypassJava9Security();
        Context context = ((JavacProcessingEnvironment) processingEnv).getContext();
        TreeMaker make = TreeMaker.instance(context);
        Names names = Names.instance(context);
        Elements elements = processingEnv.elementUtils;
        trees = Trees.instance(processingEnv);
        processors = Set.of(
                new RomanCodeTreeProcessor(make, names, elements, trees, processingEnv),
                new ForceAssertionsCodeTreeProcessor(make, names, elements, trees, processingEnv),
                new VariableTypeExpansionCodeTreeProcessor(make, names, elements, trees, processingEnv)
        );
        super.init(processingEnv);
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        if (!roundEnvironment.processingOver()) {
            for (Element element : roundEnvironment.rootElements) {
                if (element.kind.isClass) {
                    JCTree jc = (JCTree) trees.getTree(element);
                    for (CodeTreeProcessor processor : processors) {
                        jc.accept(processor);
                    }
                }
            }
        } else {
            for (CodeTreeProcessor processor : processors) {
                processor.printTally();
            }
        }
        return false;
    }
}
