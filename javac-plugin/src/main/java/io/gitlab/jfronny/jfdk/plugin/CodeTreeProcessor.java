package io.gitlab.jfronny.jfdk.plugin;

import com.sun.source.util.Trees;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.tree.TreeTranslator;
import com.sun.tools.javac.util.Names;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.util.Elements;

public abstract class CodeTreeProcessor extends TreeTranslator {
    protected final TreeMaker make;
    protected final Names names;
    protected final Elements elements;
    protected final Trees trees;
    protected final ProcessingEnvironment processingEnv;
    private final String processorName;
    protected int tally;
    public CodeTreeProcessor(TreeMaker make, Names names, Elements elements, Trees trees, ProcessingEnvironment processingEnv, String processorName) {
        this.make = make;
        this.names = names;
        this.elements = elements;
        this.trees = trees;
        this.processingEnv = processingEnv;
        this.processorName = processorName;
    }

    protected void log(String text) {
#if DEBUG
            processingEnv.messager.printMessage(javax.tools.Diagnostic.Kind.NOTE, "[$processorName] $text");
#endif
    }

    protected JCTree.JCIdent getIdentifier(Class<?> klazz) {
        for (Element element : elements.getPackageElement(klazz.getPackage().name).enclosedElements) {
            if (element.simpleName.toString().endsWith(klazz.getSimpleName()))
                return make.Ident((Symbol) element);
        }
        throw new AssertionError();
    }

    public void printTally() {
        log("Processed $tally items");
    }
}
