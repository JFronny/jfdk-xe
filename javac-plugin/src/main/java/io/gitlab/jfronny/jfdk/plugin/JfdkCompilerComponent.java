package io.gitlab.jfronny.jfdk.plugin;

import com.sun.source.tree.Tree;
import com.sun.source.util.TaskEvent;
import com.sun.source.util.TaskListener;
import com.sun.tools.javac.api.BasicJavacTask;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Log;
import com.sun.tools.javac.util.Names;
import manifold.api.type.ICompilerComponent;
import manifold.internal.javac.TypeProcessor;

import java.util.Set;

// The service file for this is missing as it didn't work
// Also it isn't up-to-date
public class JfdkCompilerComponent implements ICompilerComponent, TaskListener {
    private Set<CodeTreeProcessor> processors;
    @Override
    public void init(BasicJavacTask javacTask, TypeProcessor typeProcessor) {
        System.err.println("AAEE");
        javacTask.addTaskListener(this);
        TreeMaker make = TreeMaker.instance(javacTask.context);
        Names syms = Names.instance(javacTask.context);
        JavacElements elems = typeProcessor.elementUtil;

        processors = Set.of(
                // DO NOT ADD THINGS HERE! USE ClassProcessor!
                // THIS IS NEVER CALLED
        );

        Log.instance(javacTask.context);
    }

    @Override
    public void started(TaskEvent e) {
        System.out.println(e.kind);
    }

    @Override
    public void finished(TaskEvent e) {
        System.out.println(e.kind);
        if (e.getKind() == TaskEvent.Kind.PARSE) {
            for (Tree tree : e.compilationUnit.typeDecls) {
                if (!(tree instanceof JCTree.JCClassDecl decl)) continue;
                for (CodeTreeProcessor processor : processors) {
                    decl.accept(processor);
                }
            }
        }
    }

    @Override
    public void tailorCompiler() {
        System.err.println("TAILOR");
    }
}
