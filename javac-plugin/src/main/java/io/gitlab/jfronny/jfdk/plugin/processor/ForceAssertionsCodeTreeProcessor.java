package io.gitlab.jfronny.jfdk.plugin.processor;

import com.sun.source.util.Trees;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Names;
import io.gitlab.jfronny.jfdk.plugin.CodeTreeProcessor;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.util.Elements;

public class ForceAssertionsCodeTreeProcessor extends CodeTreeProcessor {
    public ForceAssertionsCodeTreeProcessor(TreeMaker make, Names names, Elements elements, Trees trees, ProcessingEnvironment processingEnv) {
        super(make, names, elements, trees, processingEnv, "Force Assertions");
    }

    @Override
    public void visitAssert(JCTree.JCAssert tree) {
        super.visitAssert(tree);
        List<JCTree.JCExpression> args = tree.getDetail() == null ? List.nil() : List.of(tree.detail);
        JCTree.JCExpression expr = make.NewClass(null, null, getIdentifier(AssertionError.class), args, null);
        JCTree.JCStatement newNode = make.If(make.Unary(JCTree.Tag.NOT, tree.cond), make.Throw(expr), null);
        tally++;
        result = newNode;
    }
}
