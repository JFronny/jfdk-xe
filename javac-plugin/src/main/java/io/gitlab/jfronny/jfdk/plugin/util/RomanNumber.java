package io.gitlab.jfronny.jfdk.plugin.util;

import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class RomanNumber {
    private static final Map<String, Integer> VALUES = ValueComparableMap.of(((Comparator<Integer>)Integer::compareTo).reversed(), Map.ofEntries(
            Map.entry("M", 1000),
            Map.entry("CM", 900),
            Map.entry("D", 500),
            Map.entry("CD", 400),
            Map.entry("C", 100),
            Map.entry("XC", 90),
            Map.entry("L", 50),
            Map.entry("XL", 40),
            Map.entry("X", 10),
            Map.entry("IX", 9),
            Map.entry("V", 5),
            Map.entry("IV", 4),
            Map.entry("I", 1)
    ));

    public static String toRoman(int value) {
        StringBuilder roman = new StringBuilder();
        AtomicInteger valueP = new AtomicInteger(value);
        VALUES.forEach((k, v) -> {
            while (v >= valueP.get()) {
                valueP.addAndGet(-v);
                roman.append(k);
            }
        });
        return roman.toString();
    }

    public static int toInt(String roman) {
        AtomicInteger res = new AtomicInteger(0);
        AtomicInteger i = new AtomicInteger(0);
        VALUES.forEach((k, v) -> {
            while (roman.startsWith(k, i.get())) {
                res.addAndGet(v);
                i.addAndGet(k.length());
            }
        });
        return i.get() == roman.length() ? res.get() : -1;
    }

    public static boolean isRoman(String roman) {
        return toInt(roman) != -1;
    }
}
