# JfDK
JfDK is a modified Adoptium JDK, which is set up to use manifold and some additional plugins by default.
It was designed to be used in an environment where an IDE allows specifying a JDK but no javac plugins/compiler arguments.
If you are in a normal environment, the normal [Manifold](http://manifold.systems/) plugin should be used instead.

## Downloads
- You can download a [modified JDK](https://gitlab.com/JFronny/jfdk-xe/-/jobs/artifacts/master/raw/jdk.7z?job=package)
- If you want additional messages for debugging, you can also copy the [debug patch](https://gitlab.com/JFronny/jfdk-xe/-/jobs/artifacts/master/raw/patch.7z?job=debug-patch) over it
- Docs for the JDK can be downloaded [from Oracle](https://www.oracle.com/java/technologies/javase-jdk18-doc-downloads.html), put the docs/ directory into the jdks root (where bin, conf, etc are)

## What is included?
- [Adoptium JDK](https://adoptium.net/) by Eclipse
- [Manifold](http://manifold.systems/) by [Scott McKinney](https://github.com/rsmckinney)
- [Roman numerals](https://github.com/akuhn/javac/blob/master/roman/src/numeral/RomanNumeralProcessor.java)/[assertion enforcement](https://github.com/akuhn/javac/blob/master/fa/src/ch/akuhn/fa/ForceAssertions.java) by [Adrian Kuhn](https://github.com/akuhn)
- My [java libs](https://gitlab.com/JFronny/java-commons) and Gson (under `io.gitlab.jfronny.gson`)
- dargl (developed by me for this)
- some custom additions (developed by me for this)

## Docs?
- Javadocs for the JDK can be downloaded [from Oracle](https://www.oracle.com/java/technologies/javase-jdk18-doc-downloads.html)
- Manifold is documented [on its page](http://manifold.systems/docs.html)
- My additions are documented below

## Additions
- Roman numerals are valid syntax for integers. Just type the uppercase letters for them.
- `assert` will enforce the assertion. If it is not valid, an AssertionException will be thrown
- `val` is an alias to `final var`, `let` to `var`, `bool` to `boolean`
